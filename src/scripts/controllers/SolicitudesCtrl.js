class SolicitudesCtrl {

  constructor($scope, $q, $timeout, CatalogosService, SolicitudService) {

    this.$scope = $scope;
    this.CatalogosService = CatalogosService;

    /* Se cargan los catálogos */

    this.CatalogosService.catEntidades((res) => {this.$scope.catalogos.catEntidades = res.data.response});
    this.CatalogosService.catMediosContacto((res) => {this.$scope.catalogos.catMediosContacto = res.data.response});
    this.CatalogosService.catPrioridades((res) => {this.$scope.catalogos.catPrioridades = res.data.response});
    this.CatalogosService.catCategorias((res) => {this.$scope.catalogos.catCategorias = res.data.response});
    this.CatalogosService.catClasificaciones((res) => {this.$scope.catalogos.catClasificaciones = res.data.response});
    this.CatalogosService.catSubclasificaciones((res) => {this.$scope.catalogos.catSubclasificaciones = res.data.response});
    this.CatalogosService.catCarteraServicios((res) => {this.$scope.catalogos.catCarteraServicios = res.data.response});
    this.CatalogosService.catActividadesPrincipales((res) => {this.$scope.catalogos.catActividadesPrincipales = res.data.response});
    this.CatalogosService.catParentescos((res) => {this.$scope.catalogos.catParentescos = res.data.response});
    this.showTab = false;
  }

}

SolicitudesCtrl.$inject = ['$scope', '$q', '$timeout', 'CatalogosService', 'SolicitudService'];

export default SolicitudesCtrl;
