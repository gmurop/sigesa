import { ENTIDAD_DEFAULT } from '../../constants';
import DialogPersonasCtrl from './DialogPersonasCtrl';
import generaCurp from 'curp.js';

class SolicitanteFormCtrl {

    constructor($scope, $q, $timeout, CatalogosService, SolicitudService, solicitud, $mdDialog, $mdToast, moment) {
        this.$scope = $scope;
        this.CatalogosService = CatalogosService;
        this.SolicitudService = SolicitudService;
        this.$mdDialog = $mdDialog;
        this.$mdToast = $mdToast;
        this.solicitud = solicitud;
        this.moment = moment;

        this.$scope.persona = this.$scope.solicitud.personas[this.$scope.$index];
        this.$scope.domicilio = {};

        if (this.$scope.solicitud.idSolicitud) {
            let persona = this.$scope.persona;

            this.$scope.domicilio.claveEntidad = this.$scope.catalogos.catEntidades.find(entidad => entidad.clave_entidad_federativa === persona.domicilio.claveEntidad);
            this.$scope.entidadNacimiento = this.$scope.catalogos.catEntidades.find(entidad => entidad.clave_entidad_federativa === persona.claveEntidadNacimiento);

            let municipiosPromise = $q((resolve, reject) => {
                this.CatalogosService.catMunicipios(persona.domicilio.claveEntidad, (res) => {
                    resolve(res.data.response);
                });
            });

            municipiosPromise.then(municipios => {
                this.$scope.catMunicipios = municipios;
                this.$scope.domicilio.claveMunicipio = municipios.find(municipio => municipio.clave_municipio === persona.domicilio.claveMunicipio);

                if (persona.domicilio.claveLocalidad) {
                    this.CatalogosService.catLocalidades(persona.domicilio.claveEntidad, persona.domicilio.claveMunicipio, (res) => {
                        this.$scope.catLocalidades = res.data.response;
                        this.$scope.domicilio.claveLocalidad = res.data.response.find(localidad => localidad.clave_localidad === persona.domicilio.claveLocalidad);
                    });
                }
            });
        } else {
            this.$scope.domicilio.claveEntidad = this.$scope.entidadNacimiento = this.$scope.catalogos.catEntidades.find(entidad => entidad.clave_entidad_federativa === ENTIDAD_DEFAULT);
            this.CatalogosService.catMunicipios(ENTIDAD_DEFAULT, (res) => { this.$scope.catMunicipios = res.data.response });
        }

        $scope.$watch('persona.domicilio.claveEntidad', (newValue, oldValue) => {
            let entidad = $scope.catalogos.catEntidades.find(entidad => entidad.clave_entidad_federativa === newValue)

            if ($scope.domicilio.claveEntidad) {
                $scope.persona.domicilio.entidad = entidad.entidad_federativa;
            } else {
                $scope.domicilio.claveEntidad = entidad;
            }
        });

        $scope.$watch('persona.domicilio.claveMunicipio', (newValue, oldValue) => {
            this.CatalogosService.catMunicipios($scope.persona.domicilio.claveEntidad, res => {
                let municipio = res.data.response.find(municipio => municipio.clave_municipio === newValue)

                if ($scope.domicilio.claveMunicipio) {
                    $scope.persona.domicilio.municipio = municipio.municipio;
                } else {
                    $scope.domicilio.claveMunicipio = municipio;
                }
            });
        });

        $scope.$watch('persona.domicilio.claveLocalidad', (newValue, oldValue) => {

            this.CatalogosService.catLocalidades($scope.persona.domicilio.claveEntidad, $scope.persona.domicilio.claveMunicipio, res => {
                let localidad = res.data.response.find(localidad => localidad.clave_localidad === newValue)


                if ($scope.domicilio.claveLocalidad) {
                    $scope.persona.domicilio.localidad = localidad.localidad;
                } else {
                    $scope.domicilio.claveLocalidad = localidad;
                }

            });
        });

        /* Se ligan las funciones de la clase con el scope */
        this.$scope.obtenerCatMunicipios = this.obtenerCatMunicipios.bind(this);
        this.$scope.obtenerCatLocalidades = this.obtenerCatLocalidades.bind(this);
        this.$scope.existeContactoPrincipal = this.existeContactoPrincipal.bind(this);
        this.$scope.maxDate = this.maxDate.bind(this);
        this.$scope.guardarPersona = this.guardarPersona.bind(this);
        this.$scope.habilitarBotonBuscarPersona = this.habilitarBotonBuscarPersona.bind(this);
        this.$scope.buscarPersona = this.buscarPersona.bind(this);
        this.$scope.mostrarDialogCoincidenciasPersonas = this.mostrarDialogCoincidenciasPersonas.bind(this);
        this.$scope.calcularCurp = this.calcularCurp.bind(this);
    }

    obtenerCatMunicipios(claveEntidad) {
        if (!claveEntidad) return;

        this.$scope.persona.domicilio.claveMunicipio = "";
        this.$scope.persona.domicilio.claveLocalidad = "";
        this.CatalogosService.catMunicipios(claveEntidad, (res) => { this.$scope.catMunicipios = res.data.response });
    }

    obtenerCatLocalidades(claveEntidad, claveMunicipio) {
        if (!claveEntidad || !claveMunicipio) return;
        //this.$scope.persona.domicilio.claveLocalidad = "";//TODO: Solucionar esto ya que al abrir una solicitud existente se limpia la localidad
        this.CatalogosService.catLocalidades(claveEntidad, claveMunicipio, (res) => { this.$scope.catLocalidades = res.data.response });
    }

    maxDate() {
        return newDate();
    }

    existeContactoPrincipal(solicitudIndex, personaIndex) {
        let personas = this.$scope.solicitud.personas;
        return personas.length > 0 && personas.some(persona => persona.contactoPrincipal);
    }

    guardarPersona(idSolicitud, persona) {

        let cb = res => {
            if (res.data.estatus) {
                this.$mdToast.show(
                    this.$mdToast
                    .simple()
                    .textContent('La persona ha sido actualizada.')
                    .position('bottom right')
                    .hideDelay(3000)
                )
                this.$scope.obtenerSolicitudes();
            } else {
                this.$mdToast.show(
                    this.$mdToast
                    .simple()
                    .textContent('No se pudo guardar la persona.')
                    .position('bottom right')
                    .hideDelay(3000)
                )
            }
        }

        if (persona.idPersona) {
            this.SolicitudService.guardarPersonaExistente(idSolicitud, persona, cb)
        } else {
            this.SolicitudService.guardarNuevaPersona(idSolicitud, persona, cb)
        }

    }

    buscarPersona(curp, primerApellido, segundoApellido, nombre) {
        this.SolicitudService.buscarPersona(curp, primerApellido, segundoApellido, nombre, (res => {
            this.solicitud.agregarCoincidenciasPersonas(res.data.response);
        }));
    }

    habilitarBotonBuscarPersona() {
        var { curp, primerApellido, segundoApellido, nombre } = this.$scope.persona;

        if (
            (curp.length >= 10 && curp.substr(4,6) != '000000') ||
            (primerApellido && primerApellido.length >= 3 && (segundoApellido.length >= 3 || nombre.length >= 3)) ||
            (segundoApellido && segundoApellido.length >= 3 && (primerApellido.length >= 3 || nombre.length >= 3)) ||
            (nombre && nombre.length >= 3 && primerApellido.length >= 3 || segundoApellido.length >= 3)
        ) {
            this.$scope.botonBuscarPersonaDisabled = false;
        } else {
            this.$scope.botonBuscarPersonaDisabled = true;
        }
        
    }

    mostrarDialogCoincidenciasPersonas(ev) {

        this.$mdDialog.show({
            controller: DialogPersonasCtrl,
            templateUrl: './src/partials/dialogPersonas.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            locals: {
                solicitudIndex: this.$scope.tabsSolicitudesAbiertas.selectedIndexSolicitud,
                personaIndex: this.$scope.$index
            },
            bindToController: true
        });

    }

    calcularCurp() {
        //console.log('hola');
        var { curp, primerApellido, segundoApellido, nombre, claveEntidadNacimiento, fechaNacimiento, sexo } = this.$scope.persona;

        let dia, mes, anio;
        if (fechaNacimiento) {
            anio = this.moment(fechaNacimiento).format('YYYY');
            mes = this.moment(fechaNacimiento).format('MM');
            dia = this.moment(fechaNacimiento).format('DD');
        } else {
            dia = '0';
            mes = '0';
            anio = '0000';
        }


        let objEntidad = this.$scope.catalogos.catEntidades.find(entidad => entidad.clave_entidad_federativa == claveEntidadNacimiento) || {};
        let claveInegi = objEntidad.clave_inegi;


        if (!nombre) {
            nombre = 'x';
        }

        if (!primerApellido) {
            primerApellido = 'x';
        }

        if (!segundoApellido) {
            segundoApellido = 'x';
        }

        var curpCal = generaCurp({
            nombre: nombre,
            apellido_paterno: primerApellido,
            apellido_materno: segundoApellido,
            sexo: sexo,
            estado: claveInegi,
            fecha_nacimiento: [dia, mes, anio]
        });

        this.$scope.persona.curp = curpCal;

    }
}
SolicitanteFormCtrl.$inject = ['$scope', '$q', '$timeout', 'CatalogosService', 'SolicitudService', 'solicitud', '$mdDialog', '$mdToast', 'moment'];

export default SolicitanteFormCtrl;