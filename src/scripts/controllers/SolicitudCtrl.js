import {
  ENTIDAD_DEFAULT,
  MEDIO_CONTACTO_DEFAULT,
  PRIORIDAD_DEFAULT,
  CATEGORIA_DEFAULT,
  CLASIFICACION_DEFAULT,
  CARTERA_DEFAULT
} from '../../constants';

class SolicitudCtrl {

  constructor($scope, $q, $timeout, CatalogosService, SolicitudService, solicitud, $mdToast) {

    this.$scope = $scope;
    this.$q = $q;
    this.CatalogosService = CatalogosService;
    this.SolicitudService = SolicitudService;
    this.$timeout = $timeout;
    this.solicitud = solicitud;
    this.$mdToast = $mdToast;

    this.$scope.selectedIndex = 0;

    this.$scope.solicitud = solicitud.listaSolicitudes[this.$scope.$index];

    //this.$scope.tabsContactos = this.$scope.solicitud.personas.map((persona, i) => {if (i === 0) return; }) || [];//Tabs de los contactos

    this.$scope.anteriorTab = this.anteriorTab.bind(this);
    this.$scope.siguienteTab = this.siguienteTab.bind(this);
    //this.$scope.initPersona = this.initPersona.bind(this);
    this.$scope.agregarContacto = this.agregarContacto.bind(this);
    this.$scope.eliminarContacto = this.eliminarContacto.bind(this);
    this.$scope.buscarEnCatalogo = this.buscarEnCatalogo.bind(this);
    this.$scope.nuevaSolicitud = this.nuevaSolicitud.bind(this);
    this.$scope.copiarDireccionSolicitante = this.copiarDireccionSolicitante.bind(this);
  }

  siguienteTab() {
    this.$scope.selectedIndex += 1;
  }

  anteriorTab() {
    this.$scope.selectedIndex -= 1;
  }

  agregarContacto() {

    let index = this.$scope.$index;
    let totalPersonas = this.$scope.solicitud.personas.length;
    //this.$scope.contactos.splice(1, 0, {title: 'Contacto ' + (totalPersonas)});
    this.solicitud.agregarPersona(index, false);
  }

  eliminarContacto(selectedIndexSolicitud, selectedIndex, idSolicitud = null, idPersona = null) {

    if (idSolicitud && idPersona) {

      this.SolicitudService.eliminarContacto(idSolicitud, idPersona, res => {
        this.solicitud.eliminarPersona(selectedIndexSolicitud, selectedIndex);
      });

    } else {

      this.solicitud.eliminarPersona(selectedIndexSolicitud, selectedIndex);

    }
  }

  /*
  * @fuente: Arreglo con los datos a buscar
  * @campo: Campo en el cual se va a buscar
  * @busqueda: Cadena de texto que se desea buscar
  */
  buscarEnCatalogo(fuente = {}, campo, busqueda) {
    if (typeof fuente !== 'object') return;

    let resultados = busqueda ? fuente.filter(this.createFilterFor(campo, busqueda)) : fuente,
    deffered = this.$q.defer();

    deffered.resolve(resultados);

    return deffered.promise;
  }

  /* Tomada de Angular Material */
  createFilterFor(campo, busqueda) {
   var accentMap = {
    'á':'a', 'é':'e', 'í':'i','ó':'o','ú':'u', 'A': 'Á', 'E' : 'É', 'I' : 'Í', 'O' : 'Ó', 'U' : 'Ú'
  };
  return function filterFn(entidad) {
    let entidad_federativa = angular.lowercase(entidad[campo]).split('').map(c=>{
      return accentMap[c] ? accentMap[c] : c;
    }).join('');
    return (entidad_federativa.indexOf(angular.lowercase(busqueda)) > -1);
  };

}

nuevaSolicitud(solicitud) {
  let perfilUsuarioToken = this.$scope.usuario.id_perfil_usuario;
  this.SolicitudService.nuevaSolicitud(solicitud, (res) => {
    if (res.data.estatus) {
      this.$mdToast.show(this.$mdToast.simple().textContent('La solicitud ha sido guardada.'));
      this.$scope.tabsSolicitudesAbiertas.tabs.splice(this.$scope.$index, 1);
      this.solicitud.eliminarSolicitud(this.$scope.$index);
      this.$scope.obtenerSolicitudes();
      if(perfilUsuarioToken==3){
      this.$scope.obtenerSeguimientosNoValidados();
      }else{
      this.$scope.abrirResumen('','',false);
      }
      this.$scope.obtenerContador();
    } else {
      this.$mdToast.show(this.$mdToast.simple().textContent('Ocurrió un error al guardar la solicitud.'));
    }
  });
}

  //Ésta funcion permite copiar la direccion del solicitante en el card que se ejecute
  copiarDireccionSolicitante(destino, indexPersona = 0) {
    let direccionSolicitante = this.$scope.solicitud.personas[0].domicilio;

    if (destino === 'solicitud') {
      this.$scope.solicitud.entidad = direccionSolicitante.claveEntidad;
      this.$scope.solicitud.municipio = direccionSolicitante.claveMunicipio;
      this.$scope.solicitud.localidad = direccionSolicitante.claveLocalidad;
    } else {
      this.$scope.solicitud.personas[indexPersona].domicilio = direccionSolicitante;
    }
  }

}

SolicitudCtrl.$inject = ['$scope', '$q', '$timeout', 'CatalogosService', 'SolicitudService', 'solicitud', '$mdToast'];

export default SolicitudCtrl;
