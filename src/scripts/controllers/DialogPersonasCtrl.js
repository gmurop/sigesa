class DialogPersonasCtrl {

  constructor($scope, $mdDialog, solicitud, solicitudIndex, personaIndex) {
    this.$scope = $scope;
    this.$mdDialog = $mdDialog;
    this.solicitud = solicitud;
    this.$scope.solicitudIndex = solicitudIndex;
    this.$scope.personaIndex = personaIndex;

    console.log("locals", solicitudIndex, personaIndex);

    this.$scope.coincidenciasPersonas = solicitud.coincidenciasPersonas;


    this.$scope.close = this.close.bind(this);
    this.$scope.seleccionarPersona = this.seleccionarPersona.bind(this);
  }

  close() {
    this.$mdDialog.hide();
  }

  seleccionarPersona(solicitudIndex, personaIndex, persona) {
    console.log(solicitudIndex, personaIndex);
    this.solicitud.seleccionarPersona(solicitudIndex, personaIndex, this.solicitud.generarObjetoPersona(persona, personaIndex));
    this.close();
  }


}

DialogPersonasCtrl.$inject = ['$scope', '$mdDialog', 'solicitud', 'solicitudIndex', 'personaIndex'];

export default DialogPersonasCtrl;
