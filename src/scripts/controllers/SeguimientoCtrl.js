import DialogDerivarSolicitud from './DialogDerivarSolicitud';
import DialogAgregarDocumentacion from './DialogAgregarDocumentacion';

class SeguimientoCtrl {

  constructor($scope, moment, $mdDialog, SolicitudService, $mdToast, cgNotify) {
    this.$scope = $scope;
    this.$mdDialog = $mdDialog;
    this.moment = moment;
    this.SolicitudService = SolicitudService;
    this.$mdToast = $mdToast;
    this.cgNotify = cgNotify;

    //this.$scope.solicitante = this.$scope.solicitud.personas[0];

    this.$scope.noEspecificado = 'No especificado';
    this.$scope.documentosSeguimiento = [];

    this.$scope.iconoMedioContacto = this.iconoMedioContacto.bind(this);
    this.$scope.escalarSolicitud = this.escalarSolicitud.bind(this);
    this.$scope.derivarSolicitud = this.derivarSolicitud.bind(this);
    this.$scope.finalizarSolicitud = this.finalizarSolicitud.bind(this);
    this.$scope.cancelarSolicitudEscalada = this.cancelarSolicitudEscalada.bind(this);
    this.$scope.eliminarDerivacionSolicitud = this.eliminarDerivacionSolicitud.bind(this);
    this.$scope.filtrarContactos = this.filtrarContactos.bind(this);
    this.$scope.agregarDocumentacionSoporte = this.agregarDocumentacionSoporte.bind(this);
    this.$scope.obtenerDocumentacionSoporte = this.obtenerDocumentacionSoporte.bind(this);
    this.$scope.obtenerDerivaciones = this.obtenerDerivaciones.bind(this);
    this.$scope.reabrirSolicitud = this.reabrirSolicitud.bind(this);
    this.$scope.validarSeguimiento = this.validarSeguimiento.bind(this);

    let momentCapturada = moment(this.$scope.solicitud.fechaRegistro, 'YYYY-MM-DD HH:mm:ss'),
    momentEscalada = moment(this.$scope.solicitud.resumen.fecha_solicitud_escalada, 'YYYY-MM-DD HH:mm:ss'),
    momentDerivada = moment(this.$scope.solicitud.resumen.fecha_solicitud_derivada, 'YYYY-MM-DD HH:mm:ss'),
    momentFinalizada = moment(this.$scope.solicitud.resumen.fecha_solicitud_finalizada, 'YYYY-MM-DD HH:mm:ss');

    this.$scope.fechaSolicitudCapturada = this.$scope.solicitud.fechaRegistro && momentCapturada.format('D [de] MMMM [de] YYYY');
    this.$scope.fechaSolicitudEscalada = this.$scope.solicitud.resumen.fecha_solicitud_escalada && momentEscalada.format('D [de] MMMM [de] YYYY');
    this.$scope.fechaSolicitudDerivada = this.$scope.solicitud.resumen.fecha_solicitud_derivada && momentDerivada.format('D [de] MMMM [de] YYYY');
    this.$scope.fechaSolicitudFinalizada = this.$scope.solicitud.resumen.fecha_solicitud_finalizada && momentFinalizada.format('D [de] MMMM [de] YYYY');

    this.$scope.tiempoTranscurridoCaptura = momentCapturada.fromNow();
    this.$scope.tiempoTranscurridoEscalada = momentEscalada.fromNow();
    this.$scope.tiempoTranscurridoDerivada = momentDerivada.fromNow();
    this.$scope.tiempoTranscurridoFinalizada = momentFinalizada.fromNow();

    //Se carga el seguimiento
    this.obtenerDocumentacionSoporte($scope.solicitud.idSolicitud);

    //Cargar las unidades a las que se ha derivado
    this.$scope.solicitud.derivaciones = [];
    if (this.$scope.fechaSolicitudDerivada) {
      this.obtenerDerivaciones(this.$scope.solicitud.idSolicitud);
    }
  }

  iconoMedioContacto(idMedioContacto) {
    let icono = 'call';

    switch(idMedioContacto) {
      case '1':
      icono = 'inbox';
      break;
      case '2':
      icono = 'transfer_within_a_station';
      break;
      case '3':
      icono = 'call';
      break;
      case '4':
      icono = 'mail';
      break;
      case '5':
      icono = 'domain';
      break;
      case '6':
      icono = 'domain';
      break;
    }

    return icono;

  }

  escalarSolicitud(ev, idSolicitud) {
    let confirm = this.$mdDialog.confirm()
    .title('¿Desea escalar esta solicitud?')
    .targetEvent(ev)
    .ok('Escalar')
    .cancel('Cancelar');

    this.$mdDialog.show(confirm)
    .then(() => {
      this.SolicitudService.escalarSolicitud(idSolicitud, response => {
        if (!response.data.estatus)
          return;

        this.$scope.obtenerSolicitudes();
        this.$scope.cerrarSolicitud(this.$scope.tabsSolicitudesAbiertas.selectedIndexSolicitud);
        this.$scope.obtenerContador();
        this.$scope.fechaSolicitudEscalada = this.moment().format('D [de] MMMM [de] YYYY');
        this.$scope.tiempoTranscurridoEscalada = this.moment().fromNow();
      });
    }, () => {

    });

  }

  derivarSolicitud(ev, idSolicitud, unidades, observaciones) {

    this.$mdDialog.show({
      controller: DialogDerivarSolicitud,
      templateUrl: './src/partials/dialogDerivarSolicitud.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: false,
      locals: {
        idSolicitud,
        observaciones,
        unidades
      }
    })
    .then(answer => {
      if (answer.estatus) {
        this.$scope.obtenerSolicitudes();
        this.$scope.obtenerDerivaciones(this.$scope.solicitud.idSolicitud);
        this.$scope.solicitud.observacionesDerivacion = answer.observaciones;
        this.$scope.fechaSolicitudDerivada = this.moment().format('D [de] MMMM [de] YYYY');
        this.$scope.tiempoTranscurridoDerivada = this.moment().fromNow();
      }
    }, () => {
      console.log("Derivación cancelada");
    });
  }

  eliminarDerivacionSolicitud(ev, idSolicitud, idUnidad) {

    let confirm = this.$mdDialog.confirm()
    .title('¿Desea cancelar ésta derivación?')
    .targetEvent(ev)
    .ok('Aceptar')
    .cancel('Cancelar');

    this.$mdDialog.show(confirm)
    .then(() => {
      this.SolicitudService.eliminarDerivacionSolicitud(idSolicitud, idUnidad, response => {
        if (!response.data.estatus)
          return;

        this.$scope.obtenerSolicitudes();
      });
    }, () => {
      console.log("Eliminar derivación cancelada.");
    })
  }

  finalizarSolicitud(ev, idSolicitud) {
    this.$scope.validarFinalizarSolciitud = true;
    var num = this.$scope.documentosSeguimiento;
    for (var i = 0; i < num.length; i++) {
      if((num[i].validado)==0 && !num[i].fecha_validado){
        this.$scope.validarFinalizarSolciitud = false;
      }
    }

    if(!this.$scope.validarFinalizarSolciitud){

      this.$mdDialog.show(
        this.$mdDialog.alert()
        .clickOutsideToClose(true)
        .textContent('No se puede finalizar la solicitud. Tiene seguimientos pendientes de validar.')
        .ok('Aceptar')
        .targetEvent(ev)
        );

      //this.$mdToast.show(this.$mdToast.simple().textContent());
      
    }else{
      let confirm = this.$mdDialog.confirm()
      .title('¿Desea finalizar ésta solicitud?')
      .targetEvent(ev)
      .ok('Finalizar')
      .cancel('Cancelar');

      this.$mdDialog.show(confirm)
      .then(() => {
        this.SolicitudService.finalizarSolicitud(idSolicitud, response => {
          if (!response.data.estatus)
            return;

          this.$scope.obtenerSolicitudes();
          this.$scope.solicitud.resumen.id_estatus_solicitud = 4;
          this.$scope.fechaSolicitudFinalizada = this.moment().format('D [de] MMMM [de] YYYY');
          this.$scope.tiempoTranscurridoFinalizada = this.moment().fromNow();
        });
      }, () => {
        console.log("Finalización de solicitud cancelada.");
      });
    }
  }

  cancelarSolicitudEscalada(ev, idSolicitud) {

    let confirm = this.$mdDialog.confirm()
    .title('¿Desea cancelar la escala de esta solicitud?')
    .targetEvent(ev)
    .ok('Aceptar')
    .cancel('Cancelar');

    this.$mdDialog.show(confirm)
    .then(() => {
      this.SolicitudService.cancelarSolicitudEscalada(idSolicitud, response => {
        if (!response.data.estatus)
          return;

        this.$scope.obtenerSolicitudes();
        this.$scope.cerrarSolicitud(this.$scope.tabsSolicitudesAbiertas.selectedIndexSolicitud);
        this.$scope.fechaSolicitudEscalada = null;

      });
    }, () => {
      console.log("Escala cancelada");
    });

  }

  obenerDerivaciones() {

  }

  filtrarContactos(personas) {
    return personas.filter(persona => !persona.solicitante);
  }

  agregarDocumentacionSoporte(ev, idSolicitud,idObservacion) {
    this.$mdDialog.show({
      controller: DialogAgregarDocumentacion,
      templateUrl: './src/partials/dialogAgregarDocumentacion.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: false,
      locals: {
        idSolicitud,
        idObservacion
      }
    }).then(answer => {
      if (answer.estatus) {
        //Cargar nuevamente documentación soporte
        this.$scope.obtenerSolicitudes();
        this.obtenerDocumentacionSoporte(this.$scope.solicitud.idSolicitud);
        this.$mdDialog.hide();
      }
    }, () => {
     console.log("Cancelación de agregar documento soporte");
   })

  }

  obtenerDocumentacionSoporte(idSolicitud) {
    this.SolicitudService.obtenerDocumentacionSoporte(idSolicitud, response => {
      this.$scope.documentosSeguimiento = response.data;
    });
  }

  obtenerDerivaciones(idSolicitud) {
    this.SolicitudService.obtenerDerivaciones(idSolicitud, response => {
      this.$scope.solicitud.derivaciones = response.data;
    });
  }

  reabrirSolicitud(ev, idSolicitud) {
    let confirm = this.$mdDialog.confirm()
    .title('¿Desea reabrir esta solicitud?')
    .targetEvent(ev)
    .ok('Reabrir')
    .cancel('Cancelar');

    this.$mdDialog.show(confirm)
    .then(() => {
      this.SolicitudService.reabrirSolicitud(idSolicitud, response => {
        if (!response.data.estatus)
          return;

        this.$scope.obtenerSolicitudes();
        this.$scope.solicitud.resumen.id_estatus_solicitud = 0;
        this.$scope.fechaSolicitudFinalizada = null;
        this.$scope.tiempoTranscurridoFinalizada = null;
      });
    }, () => {

    });
  }

  validarSeguimiento(ev, accion, observaciones){

    var i, titulo, mensajeB, val1, val2;

    if (accion == '1') {
      titulo = '¿Seguro desea validar el seguimiento?';
      mensajeB = 'Validado corectamente';
      val1 = 1;
      val2 = '';
    }else if(accion == '2'){
      titulo = '¿Seguro desea cancelar el seguimiento?';
      mensajeB = 'Se canceló corectamente';
      val1 = 0;
      val2 = '1';
    }else if(accion == '3'){
      titulo = '¿Seguro desea eliminar el seguimiento?';
      mensajeB = 'Se eliminó corectamente';
      val1 = -1;
      val2 = '';
    }
    let confirm = this.$mdDialog.confirm()
    .title(titulo)
    .targetEvent(ev)
    .ok('Aceptar')
    .cancel('Cancelar');

    this.$mdDialog.show(confirm)
    .then(() => {
      this.SolicitudService.validarSeguimiento(accion, observaciones.fk_id_solicitud, observaciones.id_observacion, response => {
        if(response.data){
          //this.$scope.obtenerDocumentacionSoporte(observaciones.fk_id_solicitud);
          this.$scope.obtenerSolicitudes();
          this.$mdToast.show(this.$mdToast.simple().textContent(mensajeB));
          this.$scope.obtenerSeguimientosNoValidados();
          this.obtenerDocumentacionSoporte(this.$scope.solicitud.idSolicitud);
        }else{
          this.$mdToast.show(this.$mdToast.simple().textContent('Error. No se pudo validar.'));
        }
      });
    }, () => {

    });
  }

}

SeguimientoCtrl.$inject = ['$scope', 'moment', '$mdDialog', 'SolicitudService', '$mdToast'];

export default SeguimientoCtrl;
