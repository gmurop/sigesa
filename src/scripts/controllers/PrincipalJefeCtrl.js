class PrincipalJefeCtrl {

  constructor($scope, SolicitudService, moment, NgTableParams, $q) {
    this.$scope = $scope;
  }

  //:(
  //Este controlador no tiene ninguna funcionalidad, las funciones se fueron a MainCtrl

}

PrincipalJefeCtrl.$inject = ['$scope', 'SolicitudService', 'moment', 'NgTableParams', '$q'];

export default PrincipalJefeCtrl;
