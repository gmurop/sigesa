class DialogDerivarSolicitud {
  constructor($scope, $mdDialog, CatalogosService, $q, $timeout, idSolicitud, SolicitudService, unidades, observaciones) {
    this.$scope = $scope;
    this.$q = $q;
    this.$timeout = $timeout;
    this.$mdDialog = $mdDialog;
    this.SolicitudService = SolicitudService;

    this.$scope.idSolicitud = idSolicitud;
    this.$scope.unidadesDerivar = unidades;
    this.$scope.observaciones = observaciones;

    this.$scope.close = this.close.bind(this);
    this.$scope.derivar = this.derivar.bind(this);
    this.$scope.query = this.query.bind(this);
    this.$scope.agregarUnidadDerivar = this.agregarUnidadDerivar.bind(this);
    this.$scope.quitarUnidadDerivar = this.quitarUnidadDerivar.bind(this);

    CatalogosService.catUnidadesAdministrativas(response => {
      this.$scope.unidadesAdministrativas = response.data.response;
    });
  }

  query(searchText) {
    let d = this.$q.defer(),
        unidades;

    this.$timeout(() => {
      unidades = this.$scope.unidadesAdministrativas
        .filter(unidad => angular.lowercase(unidad.descripcion_unidad_administrativa).indexOf(angular.lowercase(searchText)) > -1);

      d.resolve(unidades);
    });

    return d.promise;
  }

  close(answer = false) {
    this.$mdDialog.hide(answer);
  }

  createFilterFor(query) {
    var lowercaseQuery = angular.lowercase(query);

    return function filterFn(state) {
      return (state.value.indexOf(lowercaseQuery) === 0);
    };

  }

  agregarUnidadDerivar(unidad) {
    if (!unidad || this.$scope.unidadesDerivar.some(u => u.id_unidad_administrativa === unidad.id_unidad_administrativa)) return;

    this.$scope.unidadesDerivar.push(unidad);
    this.$scope.unidadDerivar = '';
  }

  quitarUnidadDerivar(index) {
    this.$scope.unidadesDerivar.splice(index, 1);
  }

  derivar(idSolicitud, unidades, observaciones) {
    this.SolicitudService.derivarSolicitud(idSolicitud, unidades, observaciones, response => {
      if (!response.data.estatus)
        return;

      this.close({
        estatus: response.data.estatus,
        observaciones
      });
    });
  }
}

DialogDerivarSolicitud.$inject = ['$scope', '$mdDialog', 'CatalogosService', '$q', '$timeout', 'idSolicitud', 'SolicitudService', 'unidades', 'observaciones'];

export default DialogDerivarSolicitud;
