class LoginCtrl {

  //Inyectar el servicio de local storage
  constructor($rootScope, $scope, LoginService, localStorageService, $state, $mdDialog) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.$mdDialog = $mdDialog;
    this.LoginService = LoginService;
    this.localStorage = localStorageService;

    this.$scope.login = this.login.bind(this);
  }

  login(username, password) {
    this.LoginService.login(username, password, result => {

      if (result.data.estatus) {
        //Crear cookie con el token
        let token = result.data.token;
        this.localStorage.set('token', token);
        //Guardar token
        this.$state.go('inicio');
      } else {
        //Mensaje de alerta al usuario para que se vuelva a loguear
        this.$mdDialog.show(
          this.$mdDialog.alert()
            .parent(angular.element(document.body))
            .clickOutsideToClose(true)
            .title('Usuario o contraseña incorrecto.')
            .textContent('Verifique sus datos y vuelva a intentarlo.')
            .ok('Entendido')
        )
      }

      this.$rootScope.loggedIn = result.data.estatus;
    })
  }

}

LoginCtrl.$inject = ['$rootScope', '$scope', 'LoginService', 'localStorageService', '$state', '$mdDialog'];

export default LoginCtrl;
