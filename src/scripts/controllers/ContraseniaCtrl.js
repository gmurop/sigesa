class ContraseniaCtrl {

    constructor($scope, SolicitudService, moment, $mdToast, $mdDialog) {
        this.$scope = $scope;
        this.SolicitudService = SolicitudService;
        this.moment = moment;
        this.$mdToast = $mdToast;
        this.$mdDialog = $mdDialog;

        this.$scope.passwordChanged = false;
        this.$scope.passwordValid = false;
        this.$scope.mensaje = {};

        this.$scope.validatePassword = this.validatePassword.bind(this);
        this.$scope.changePassword = this.changePassword.bind(this);
    }

    validatePassword(newPassword, confirmNewPassword) {
        this.$scope.passwordValid = (newPassword === confirmNewPassword) ? true : false;
    }

    changePassword(password, newPassword) {
    	 this.SolicitudService.cabiarContrasenia(password, newPassword, res => {
    	 	if(res.data.estatus == 1){
    	 		this.$scope.mensaje = {
    	 			color: 'green',
    	 			texto: "Se cambio la contraseña correctamente.",
    	 			icono: 'done'
    	 		}
                //this.$mdDialog.hide();
    	 	}else if(res.data.estatus == 2){
    	 		this.$scope.mensaje = {
    	 			color: 'orange',
    	 			texto: 'La contraseña anterior es incorrecta.',
    	 			icono: 'warning'
    	 		}
    	 	}else{
    	 		this.$scope.mensaje = {
    	 			color: 'red',
    	 			texto: "Error! no se cambio la contraseña, intente de nuevo.",
    	 			icono: 'clear'
    	 		}
                console.log(res.data.mensaje);
    	 	}
        });
    	
    }
}

ContraseniaCtrl.$inject = ['$scope', 'SolicitudService', 'moment', '$mdToast', '$mdDialog'];

export default ContraseniaCtrl;