class MainCtrl {

    constructor($scope, $timeout, SolicitudService, solicitud, localStorageService, $state, moment, $q, NgTableParams, $mdDialog, $rootScope, $http, notify, $mdToast) {
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.SolicitudService = SolicitudService;
        this.$scope.solicitud = solicitud;
        this.localStorageService = localStorageService;
        this.$state = $state;
        this.moment = moment;
        this.$q = $q;
        this.NgTableParams = NgTableParams;
        this.$mdDialog = $mdDialog;
        this.$rootScope = $rootScope;
        this.$http = $http;
        this.notify = notify;
        this.$mdToast = $mdToast;
        this.$scope.tabsSolicitudesAbiertas = {
            tabs: [],
            selectedIndexSolicitud: 0
        };
        this.$scope.procesos = {
            abriendoSolicitud: false
        };
        this.$scope.estatuses = [];
        this.$scope.catalogos = {
            catEntidades: [],
            catMediosContacto: [],
            catPrioridades: [],
            catCategorias: [],
            catClasificaciones: [],
            catSubclasificaciones: [],
            catCarteraServicios: [],
            catActividadesPrincipales: [],
            catParentescos: [],
            catSemaforos: [{
                    id: 1,
                    color: 'green',
                    descripcion: 'MENOR O IGUAL A 3 DÍAS'
                },
                {
                    id: 2,
                    color: 'orange',
                    descripcion: 'ENTRE 4 Y 7 DÍAS'
                },
                {
                    id: 3,
                    color: 'red',
                    descripcion: 'MAYOR A 7 DÍAS'
                },
                {
                    id: 4,
                    color: '#294986',
                    descripcion: 'FINALIZADAS'
                }
            ],
            catMeses: [{
                    num: 1,
                    mes: 'ENERO'
                },
                {
                    num: 2,
                    mes: 'FEBRERO'
                },
                {
                    num: 3,
                    mes: 'MARZO'
                },
                {
                    num: 4,
                    mes: 'ABRIL'
                },
                {
                    num: 5,
                    mes: 'MAYO'
                },
                {
                    num: 6,
                    mes: 'JUNIO'
                },
                {
                    num: 7,
                    mes: 'JULIO'
                },
                {
                    num: 8,
                    mes: 'AGOSTO'
                },
                {
                    num: 9,
                    mes: 'SEPTIEMBRE'
                },
                {
                    num: 10,
                    mes: 'OCTUBRE'
                },
                {
                    num: 11,
                    mes: 'NOVIEMBRE'
                },
                {
                    num: 12,
                    mes: 'DICIEMBRE'
                }
            ]
        };

        $scope.tableParams = this.setTableParams();
        $scope.tableParamsReportes = this.setTableParamsReportes();
        $scope.filtrarParams = {};
        $scope.filtrarParamsReportes = {};
        $scope.hoy = new Date();
        $scope.mes = $scope.hoy.getMonth() + 1;

        $scope.maxDate = new Date(
            $scope.hoy.getFullYear(),
            $scope.hoy.getMonth(),
            $scope.hoy.getDate());

        this.obtenerContador();
        this.obtenerSeguimientosNoValidados();
        this.filtarTablaGraficas(this.$scope.mes);

        this.$scope.clasificarSolicitudes = this.clasificarSolicitudes.bind(this);
        this.$scope.filtrarSolicitudes = this.filtrarSolicitudes.bind(this);
        this.$scope.obtenerSolicitudes = this.obtenerSolicitudes.bind(this);
        this.$scope.crearSolicitud = this.crearSolicitud.bind(this);
        this.$scope.cerrarSolicitud = this.cerrarSolicitud.bind(this);
        this.$scope.abrirSolicitud = this.abrirSolicitud.bind(this);
        this.$scope.colorPrioridad = this.colorPrioridad.bind(this);
        this.$scope.colorSemaforo = this.colorSemaforo.bind(this);
        this.$scope.colorSemaforoSUG = this.colorSemaforoSUG.bind(this);
        this.$scope.logout = this.logout.bind(this);
        this.$scope.obtenerContador = this.obtenerContador.bind(this);
        //this.$scope.filtrarSolicitudes = this.filtrarSolicitudes.bind(this);
        this.$scope.abrirResumen = this.abrirResumen.bind(this);
        this.$scope.abrirResumenJefe = this.abrirResumenJefe.bind(this);
        this.$scope.obtenerSeguimientosNoValidados = this.obtenerSeguimientosNoValidados.bind(this);
        this.$scope.opcionesReportes = this.opcionesReportes.bind(this);
        this.$scope.reporteOperativoExcel = this.reporteOperativoExcel.bind(this);
        this.$scope.abrirReporte1 = this.abrirReporte1.bind(this);
        this.$scope.filtrarSolicitudesReportes = this.filtrarSolicitudesReportes.bind(this);
        this.$scope.recargarImagen = this.recargarImagen.bind(this);
        this.$scope.filtarTablaGraficas = this.filtarTablaGraficas.bind(this);
        this.$scope.reporteSeguimientoExcel = this.reporteSeguimientoExcel.bind(this);
        this.$scope.imprimirDiv = this.imprimirDiv.bind(this);
        this.$scope.agregarFusa = this.agregarFusa.bind(this);
        this.$scope.cambiarContrasenia = this.cambiarContrasenia.bind(this);

    }

    cambiarContrasenia(ev) {
        this.$mdDialog.show({
            controller: 'ContraseniaCtrl',
            templateUrl: 'src/partials/contraseniaForm.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            scope: this.$scope,
            preserveScope: true
        });
    }

    agregarFusa(solicitud, fusa) {
        this.SolicitudService.agregarFusa(solicitud.idSolicitud, fusa, res => {

            if (res.data.estatus) {
                this.$mdToast.show(
                    this.$mdToast
                    .simple()
                    .textContent('Se agrego correctamente el FUSA.')
                    .position('top right')
                    .hideDelay(3000)
                );

                var datos = {
                    fk_id_solicitud: solicitud.idSolicitud,
                    fecha_registro_plataforma: fusa.fechaRegistroFusa,
                    folio_plataforma: fusa.folioFusa
                };

                this.$scope.solicitud.listaSolicitudes[this.$scope.tabsSolicitudesAbiertas.selectedIndexSolicitud].fusa = datos;

                this.$scope.obtenerContador();
            } else {
                this.$mdToast.show(
                    this.$mdToast
                    .simple()
                    .textContent('Error al guardar el FUSA.')
                    .position('top right')
                    .hideDelay(3000)
                );
            }


        });
    }

    imprimirDiv(ev, printSectionId) {
        console.log("id", printSectionId);
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        var urlEstilo = this.$rootScope.baseUrl + "sigesa/dist/css/main.css";
        var estiloSeguimiento = this.$rootScope.baseUrl + "sigesa/dist/css/estilosReportesImprimir.css";        
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="' + urlEstilo + '" /><link rel="stylesheet" type="text/css" href="' + estiloSeguimiento + '" /><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    }

    recargarImagen(ev, mes) {
        if (!mes) {
            mes = this.$scope.mes;
        }
        if (this.$scope.usuario.id_perfil_usuario == 3) {
            var parametro = ev.timeStamp;
            document.getElementById('grafica1').src = this.$rootScope.baseUrl + "api/sigesa/graficas/1/mes/" + mes + "/" + parametro;
            document.getElementById('grafica2').src = this.$rootScope.baseUrl + "api/sigesa/graficas/2/mes/" + mes + "/" + parametro;
            document.getElementById('grafica3').src = this.$rootScope.baseUrl + "api/sigesa/graficas/3/mes/" + mes + "/" + parametro;
            document.getElementById('grafica4').src = this.$rootScope.baseUrl + "api/sigesa/graficas/4/mes/" + mes + "/" + parametro;
            document.getElementById('grafica5').src = this.$rootScope.baseUrl + "api/sigesa/graficas/5/mes/" + mes + "/" + parametro;
            document.getElementById('grafica6').src = this.$rootScope.baseUrl + "api/sigesa/graficas/6/mes/" + mes + "/" + parametro;
            document.getElementById('grafica7').src = this.$rootScope.baseUrl + "api/sigesa/graficas/7/mes/" + mes + "/" + parametro;
            this.filtarTablaGraficas(mes);
            document.getElementById('grafica9').src = this.$rootScope.baseUrl + "api/sigesa/graficas/9/mes/" + mes + "/" + parametro;
            document.getElementById('grafica10').src = this.$rootScope.baseUrl + "api/sigesa/graficas/10/mes/" + mes + "/" + parametro;
            document.getElementById('grafica11').src = this.$rootScope.baseUrl + "api/sigesa/graficas/11/mes/" + mes + "/" + parametro;
        }

    }

    reporteOperativoExcel(ev, parametros) {
        let fechaInicial = this.moment(parametros.fechaInicial, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
        let fechaFinal = this.moment(parametros.fechaFinal, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
        location.href = this.$rootScope.baseUrl + "api/sigesa/reportes/operativo/excel/" + fechaInicial + "/" + fechaFinal;
    }

    reporteSeguimientoExcel(ev, parametros) {
        this.SolicitudService.obtenerReporteSeguimientoExcel(parametros, response => {
            //console.log(response);
            location.href = this.$rootScope.baseUrl+"api/tmp/"+response.data.archivo;
        });
    }

    obtenerSolicitudes(filtro = '') {
        this.SolicitudService.obtenerSolicitudes((res => {
            this.$scope.estatuses = [
                { id: 1, descripcion: 'SEGUIMIENTO', solicitudes: [] },
                { id: 2, descripcion: 'ESCALADAS', solicitudes: [] },
                { id: 3, descripcion: 'DERIVADAS', solicitudes: [] },
                { id: 4, descripcion: 'FINALIZADAS', solicitudes: [] }
            ];

            //Se procesan las solicitudes antes de clasificarlas
            let solicitudes = res.data.response.map(solicitud => {
                solicitud.fechaRegistro = this.moment(solicitud.fecha_registro, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
                solicitud.fechaUltimoSeguimiento = this.moment(solicitud.fecha_ultimo_seguimiento, 'YYYY-MM-DD HH:mm:ss').fromNow();

                return solicitud;
            });

            this.clasificarSolicitudes(this.$scope.estatuses, solicitudes);
        }));
    }

    clasificarSolicitudes(estatuses, solicitudes) {
        estatuses.forEach(estatus => {
            estatus.solicitudes = solicitudes.filter(solicitud => Number(solicitud.id_estatus_solicitud) === estatus.id);
        });
    }

    filtrarSolicitudes(idEstatus) {
        return this.$scope.solicitudes.map(solicitud => Number(solicitud.id_estatus_solicitud) === idEstatus);
    }

    crearSolicitud(label = 'Nueva', nueva = true) {
        this.$scope.tabsSolicitudesAbiertas.tabs.push({ label: label });

        if (!nueva) return;
        this.$scope.solicitud.agregarSolicitud();
    }

    cerrarSolicitud(index) {
        this.$scope.tabsSolicitudesAbiertas.tabs.splice(index, 1);
        this.$scope.solicitud.eliminarSolicitud(index);
        this.$scope.procesos.abriendoSolicitud = false;
    }

    abrirSolicitud(idSolicitud, solicitudResumen, actualizar = false) {
        //Verificar que no esté abierta
        let abierta = false;

        this.$scope.tabsSolicitudesAbiertas.tabs.forEach((tab, i) => {
            if (tab.label === '#' + solicitudResumen.ticket) {
                abierta = true;
                this.$scope.tabsSolicitudesAbiertas.selectedIndexSolicitud = i;
                return;
            }
        });

        if (abierta && !actualizar) return;

        this.SolicitudService.obtenerSolicitudes((res => {
            let response = res.data.response;
            this.crearSolicitud('#' + solicitudResumen.ticket, false);

            response.personasTerceros.splice(0, 0, response.solicitante); //Se agrega el solicitante en la posición 0
            //console.log("personas", response.personasTerceros);
            let solicitud = {
                idSolicitud: Number(idSolicitud),
                ticket: response.solicitud.ticket,
                fechaEvento: new Date(response.solicitud.fecha_evento),
                unidadMedica: response.solicitud.fk_clues_evento,
                unidadMedicaDescripcion: response.solicitud.unidad_salud,
                entidad: response.solicitud.fk_clave_entidad_federativa,
                entidadDescripcion: response.solicitud.entidad_federativa,
                municipio: response.solicitud.fk_clave_municipio,
                municipioDescripcion: response.solicitud.municipio,
                localidad: response.solicitud.fk_clave_localidad,
                localidadDescripcion: response.solicitud.localidad,
                medioContacto: response.solicitud.fk_id_medio_contacto,
                medioContactoDescripcion: response.solicitud.medio_contacto,
                prioridad: response.solicitud.fk_id_prioridad_solicitud,
                prioridadDescripcion: response.solicitud.prioridad_solicitud,
                descripcion: (response.descripcion_solicitud ? response.descripcion_solicitud.observacion : 'No se ha capturado una descripción de la solicitud.'),
                clasificacion: response.solicitud.fk_id_clasificacion_sug,
                clasificacionDescripcion: response.solicitud.clasificacion_sug,
                subclasificacion: response.solicitud.fk_id_subclasificacion_sug,
                subclasificacionDescripcion: response.solicitud.subclasificacion_sug,
                categoria: response.solicitud.fk_id_categoria_sug,
                categoriaDescripcion: response.solicitud.categoria_sug,
                cartera: response.solicitud.fk_id_cartera_servicios_medicos,
                carteraDescripcion: response.solicitud.cartera_servicios_medicos,
                intervencion: response.solicitud.fk_id_intervencion_carteras_servicios_medicos,
                intervencionDescripcion: response.solicitud.intervencion_cartera_servicios_medicos,
                actividadPrincipal: response.solicitud.fk_id_actividad_principal,
                actividadPrincipalDescripcion: response.solicitud.actividad_principal,
                actividadesSecundarias: response.actividadesSecundarias.map(a => ({ actividad: a.id_actividad_secundaria_solicitud, nombre: a.actividad_secundaria })),
                personas: response.personasTerceros.map((persona, i) => (this.$scope.solicitud.generarObjetoPersona(persona, i))),
                fechaRegistro: response.solicitud.fecha_registro,
                usuarioRegistro: response.solicitud.usuario_registro,
                observacionesDerivacion: response.solicitud.observaciones_derivacion,
                resumen: solicitudResumen,
                fusa: response.fusa
            }
            //console.log(solicitud);
            this.$scope.procesos.abriendoSolicitud = false;
            this.$scope.solicitud.agregarSolicitud(solicitud);
        }), null, idSolicitud);
    }

    colorPrioridad(idPrioridad) {
        let color;

        switch (idPrioridad) {
            case '1':
                color = 'green';
                break;
            case '2':
                color = 'red';
                break;
            case '3':
                color = 'orange';
                break;
            default:
                color = 'blue';
        }
        return color;
    }

    colorSemaforo(ultimo_seguimiento) {
        let diasTranscurridos = this.moment().diff(this.moment(ultimo_seguimiento, 'YYYY-MM-DD'), 'days');
        let color;

        switch (true) {
            case diasTranscurridos <= 3:
                color = 'green';
                break;

            case diasTranscurridos >= 4 && diasTranscurridos <= 7:
                color = 'orange';
                break;

            case diasTranscurridos >= 8:
                color = 'red';
                break;

            default:
                color = 'darkgrey';
        }

        return color;

    }
    colorSemaforoSUG(dias) {
        let color;

        switch (true) {
            case dias <= 90:
                color = 'orange';
                break;

            case dias > 90:
                color = 'red';
                break;

            default:
                color = 'darkgrey';
        }

        return color;
    }

    logout() {
        this.localStorageService.remove('token');
        this.$state.go('login');
    }

    obtenerContador() {
        this.SolicitudService.obtenerContadorSolicitudes(response => {
            this.$scope.contadores = response.data;
        });
    }

    obtenerSeguimientosNoValidados() {
        this.SolicitudService.obtenerSeguimientosNoValidadosSolicitudes(response => {
            this.$scope.seguimientosNoValidados = response.data;
        });
    }

    filtrarSolicitudes(...filtrarParams) {
        this.$scope.tableParams = this.setTableParams(...filtrarParams);
    }

    setTableParams(filtrarParams) {
        console.log(filtrarParams);
        return new this.NgTableParams({ count: 100 }, {
            counts: [],
            getData: params => {
                let ordenar, direccionOrdenar,
                    sorting = params.sorting();
                for (let param in sorting) {
                    ordenar = param;
                    direccionOrdenar = sorting[param];
                }
                let p = this.$q.defer();
                this.SolicitudService.obtenerSolicitudes(response => {
                    params.total(response.data.total);
                    p.resolve(response.data.response);
                    console.log("datos",p);
                }, {
                    pagina: params.page(),
                    porPagina: params.count(),
                    ordenar,
                    direccionOrdenar,
                    ...filtrarParams
                });
                return p.promise.then(data => data);
            }
        });
    }

    filtrarSolicitudesReportes(...filtrarParamsReportes) {
        this.$scope.tableParamsReportes = this.setTableParamsReportes(...filtrarParamsReportes);
    }

    setTableParamsReportes(filtrarParamsReportes) {
        return new this.NgTableParams({ count: 100 }, {
            counts: [],
            getData: params => {
                let ordenar, direccionOrdenar,
                    sorting = params.sorting();
                for (let param in sorting) {
                    ordenar = param;
                    direccionOrdenar = sorting[param];
                }
                let p = this.$q.defer();
                this.SolicitudService.obtenerDesgloceIncidencias(response => {

                    //params.total(response.data.total);
                    p.resolve(response.data);
                }, {
                    pagina: params.page(),
                    porPagina: params.count(),
                    ordenar,
                    direccionOrdenar,
                    ...filtrarParamsReportes
                });
                //console.log(data);
                return p.promise.then(data => data);
            }
        });
    }

    filtarTablaGraficas(mes) {
        var parametro = event.timeStamp;
        this.SolicitudService.obtenerTablaGrafica(mes, parametro, response => {
            this.$scope.datosTablaGrafica = response.data;
        });
    }

    abrirResumen(ev, semaforo, valor = true) {
        let perfil = this.$scope.usuario.id_perfil_usuario;
        const tabs = this.$scope.tabsSolicitudesAbiertas.tabs.length;

        if ((tabs > 0 && valor) || perfil == 3) {
            //Abrir en dialog
            this.$mdDialog.show({
                controller: 'ResumenCtrl',
                templateUrl: 'src/partials/resumen.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                scope: this.$scope,
                preserveScope: true
            });
        }
        this.filtrarSolicitudes({ semaforo: semaforo });
    }

    abrirResumenJefe(ev) {
        const tabs = this.$scope.tabsSolicitudesAbiertas.tabs.length;

        if (tabs > 0) {
            //Abrir en dialog
            this.$mdDialog.show({
                controller: 'ResumenCtrl',
                templateUrl: 'src/partials/principalJefe.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                scope: this.$scope,
                preserveScope: true
            });
        }

    }

    opcionesReportes(ev) {
        //Abrir en dialog
        this.$mdDialog.show({
            controller: 'OpcionesReportesCtrl',
            templateUrl: 'src/partials/opcionesReportes.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            scope: this.$scope,
            preserveScope: true
        });

    }

    abrirReporte1(nombre) {
        let abierta = false;
        this.$scope.tabsSolicitudesAbiertas.tabs.forEach((tab, i) => {

            if (tab.idLabel === 'R1') {
                abierta = true;
                this.$scope.tabsSolicitudesAbiertas.selectedIndexSolicitud = i;
                return;
            }
        });

        if (abierta) {
            this.$mdDialog.hide();
            return;
        } else {
            this.$scope.tabsSolicitudesAbiertas.tabs.push({ label: nombre, reporte: true, idLabel: 'R1' });
            this.$scope.solicitud.agregarSolicitud();
        }
        this.$mdDialog.hide();
    }

}

MainCtrl.$inject = ['$scope', '$timeout', 'SolicitudService', 'solicitud', 'localStorageService', '$state', 'moment', '$q', 'NgTableParams', '$mdDialog', '$rootScope', '$http', 'notify', '$mdToast'];

export default MainCtrl;