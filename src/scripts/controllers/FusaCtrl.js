class FusaCtrl {

  constructor($scope, SolicitudService, moment) {
    this.$scope = $scope;
    this.SolicitudService = SolicitudService;
    this.moment = moment;
  }
  //:(
  //Este controlador no tiene ninguna funcionalidad, las funciones se fueron a MainCtrl

}

FusaCtrl.$inject = ['$scope', 'SolicitudService', 'moment'];

export default FusaCtrl;
