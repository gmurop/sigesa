class FiltrarSolicitudesCtrl {

  constructor($scope, SolicitudService) {
    this.$scope = $scope;
    this.SolicitudService = SolicitudService;

    this.$scope.obtenerSolicitudes();
    this.$scope.buscarSolicitudes = this.buscarSolicitudes.bind(this);
  }

  buscarSolicitudes(search) {
    if (search === '') {
      this.$scope.obtenerSolicitudes();
    } else {
      this.SolicitudService.buscarSolicitudes(search, res => {
        this.$scope.clasificarSolicitudes(this.$scope.estatuses, res.data.response);
      });
    }
  }

}

FiltrarSolicitudesCtrl.$inject = ['$scope', 'SolicitudService'];

export default FiltrarSolicitudesCtrl;
