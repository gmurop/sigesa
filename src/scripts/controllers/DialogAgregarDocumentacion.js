class DialogAgregarDocumentacion {

  constructor($scope, idSolicitud, idObservacion, $mdDialog, SolicitudService, $mdToast) {
    this.$scope = $scope;
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.SolicitudService = SolicitudService;

    this.$scope.idSolicitud = idSolicitud;
    this.$scope.editarObservacion = {};
    this.$scope.estatusEditarObservacion = false;

    if(idObservacion){
      this.$scope.estatusEditarObservacion = true;

      this.SolicitudService.obtenerObservacionSeguimiento(idSolicitud, idObservacion, response => {
        this.$scope.editarObservacion = {id_observacion : response.data.id_observacion, observacion : response.data.observacion}
      });
    }
    
    this.$scope.agregarDocumentacionSoporte = this.agregarDocumentacionSoporte.bind(this);
    this.$scope.editarDocumentacionSoporte = this.editarDocumentacionSoporte.bind(this);
    this.$scope.close = this.close.bind(this);

  }

  agregarDocumentacionSoporte(idSolicitud, observacion, archivos = []) {
    this.SolicitudService.agregarDocumentacion(idSolicitud, observacion, archivos, response => {
      if (response.data.estatus) {
        this.$mdToast.show(this.$mdToast.simple().textContent('La documentación ha sido guardada.'));
        this.close({estatus: true});
      }
    });
  }

  editarDocumentacionSoporte(idSolicitud, idObservacion, observacion, archivos = []) {
    this.SolicitudService.editarDocumentacion(idSolicitud, idObservacion, observacion, archivos, response => {
      if (response.data.estatus) {
        this.$mdToast.show(this.$mdToast.simple().textContent('El seguimiento ha sido actualizado.'));
        this.close({estatus: true});
      }else{
        this.$mdToast.show(this.$mdToast.simple().textContent('Error al actualizar seguimiento'));
      }
    });
    
  }

  close(answer = {estatus: false}) {
    this.$mdDialog.hide(answer);
  }

}

DialogAgregarDocumentacion.$inject = ['$scope', 'idSolicitud', 'idObservacion', '$mdDialog', 'SolicitudService', '$mdToast'];

export default DialogAgregarDocumentacion;
