import { ENTIDAD_DEFAULT } from '../../constants';

class SolicitudFormCtrl {

  constructor($scope, CatalogosService, SolicitudService, $mdToast) {

    this.$scope = $scope;
    this.CatalogosService = CatalogosService;
    this.SolicitudService = SolicitudService;
    this.$mdToast = $mdToast;

    this.$scope.obtenerCatMunicipios = this.obtenerCatMunicipios.bind(this);
    this.$scope.obtenerCatLocalidades = this.obtenerCatLocalidades.bind(this);
    this.$scope.obtenerCatUnidadesMedicas = this.obtenerCatUnidadesMedicas.bind(this);
    this.$scope.obtenerCatIntervenciones = this.obtenerCatIntervenciones.bind(this);
    this.$scope.obtenerCatActividadesSecundarias = this.obtenerCatActividadesSecundarias.bind(this);
    this.$scope.actualizarSolicitud = this.actualizarSolicitud.bind(this);

    let solicitud = this.$scope.solicitud;

    //console.log(solicitud);

    if (solicitud.idSolicitud) {

        this.CatalogosService.catMunicipios(solicitud.entidad, (res) => {
          this.$scope.catMunicipios = res.data.response;
          this.$scope.municipio = res.data.response.find(municipio => municipio.clave_municipio === solicitud.municipio);

          if (solicitud.localidad) {
            this.CatalogosService.catLocalidades(solicitud.entidad, solicitud.municipio, (res) => {
              this.$scope.catLocalidades = res.data.response;
              this.$scope.localidad = res.data.response.find(localidad => localidad.clave_localidad === solicitud.localidad);
            });
          }

          if (solicitud.unidadMedica) {
            this.CatalogosService.catUnidadesMedicas(solicitud.entidad, solicitud.municipio, (res) => {
              this.$scope.catUnidadesMedicas = res.data.response;
              this.$scope.unidadMedica = res.data.response.find(unidad => unidad.clues === solicitud.unidadMedica);
            });
          }
        });
    } else {
      this.$scope.entidad = this.$scope.catalogos.catEntidades.find(entidad => entidad.clave_entidad_federativa === ENTIDAD_DEFAULT);
      this.CatalogosService.catMunicipios(ENTIDAD_DEFAULT, (res) => {this.$scope.catMunicipios = res.data.response});
    }

    $scope.$watch('solicitud.entidad', (newValue, oldValue) => {
      if ($scope.entidad) return;

      $scope.entidad = $scope.catalogos.catEntidades.find(entidad => entidad.clave_entidad_federativa === newValue)
    });

    $scope.$watch('solicitud.municipio', (newValue, oldValue) => {
      if ($scope.municipio) return;

      this.CatalogosService.catMunicipios($scope.solicitud.entidad, res => {
        $scope.municipio = res.data.response.find(municipio => municipio.clave_municipio === newValue)
      });
    });

    $scope.$watch('solicitud.localidad', (newValue, oldValue) => {
      if ($scope.localidad) return;

      this.CatalogosService.catLocalidades($scope.solicitud.entidad, $scope.solicitud.municipio, res => {
        $scope.localidad = res.data.response.find(localidad => localidad.clave_localidad === newValue)
      });
    });

    if (solicitud.actividadPrincipal) {
      this.obtenerCatActividadesSecundarias(solicitud.actividadPrincipal);
    }

    if (solicitud.cartera) {
      this.CatalogosService.catIntervenciones(solicitud.cartera, (res) => {
        this.$scope.catIntervenciones = res.data.response;

        if (solicitud.intervencion) {
          this.$scope.intervencion = res.data.response.find(intervencion => intervencion.id_intervencion_carteras_servicios_medicos === solicitud.intervencion);
        }
      });
    }
  }

  obtenerCatMunicipios(claveEntidad) {
    if (!claveEntidad) return;

    //this.$scope.municipio = "";
    //this.$scope.localidad = "";
    //this.$scope.unidadMedica = "";
    this.CatalogosService.catMunicipios(claveEntidad, (res) => {this.$scope.catMunicipios = res.data.response});
  }

  obtenerCatLocalidades(claveEntidad, claveMunicipio) {
    if (!claveEntidad|| !claveMunicipio) return;

    //this.$scope.localidad = "";
    this.CatalogosService.catLocalidades(claveEntidad, claveMunicipio, (res) => {this.$scope.catLocalidades = res.data.response});
  }

  obtenerCatUnidadesMedicas(claveEntidad, claveMunicipio) {
    if (!claveEntidad|| !claveMunicipio) return;

    //this.$scope.unidadMedica = "";
    this.CatalogosService.catUnidadesMedicas(claveEntidad, claveMunicipio, (res) => {this.$scope.catUnidadesMedicas = res.data.response});
  }

  obtenerCatIntervenciones(idCartera) {
    this.CatalogosService.catIntervenciones(idCartera, (res) => this.$scope.catIntervenciones = res.data.response);
  }

  obtenerCatActividadesSecundarias(idActividadPrimaria) {
    this.CatalogosService.catActividadesSecundarias(idActividadPrimaria, (res) => this.$scope.catActividadesSecundarias = res.data.response);
  }

  actualizarSolicitud(solicitud) {
    this.SolicitudService.actualizarSolicitud(solicitud, res => {
      if (res.data.estatus) {
        this.$mdToast.show(
          this.$mdToast
          .simple()
          .textContent('La solicitud ha sido actualizada.')
          .position('bottom right')
          .hideDelay(3000)
        );
        this.$scope.obtenerSolicitudes();
        this.$scope.abrirSolicitud(solicitud.idSolicitud,solicitud.resumen,true);
        this.$scope.cerrarSolicitud(this.$scope.tabsSolicitudesAbiertas.selectedIndexSolicitud);
        this.$scope.obtenerContador();
      } else {
        this.$mdToast.show(
          this.$mdToast
          .simple()
          .textContent('Error al guardar la solicitud.')
          .position('bottom right')
          .hideDelay(3000)
        );
      }
    });
  }

}

SolicitudFormCtrl.$inject = ['$scope', 'CatalogosService', 'SolicitudService', '$mdToast'];

export default SolicitudFormCtrl;
