let Run = ($rootScope, localStorageService, $state, LoginService, $timeout, $transitions, $http, $q, $location) => {

  let absUrl = $location.absUrl();
  switch (true) {
      case absUrl.startsWith('http://187.217.116.231/testing/'):
          $rootScope.ENVIRONMENT = 'testing';
          $rootScope.baseUrl = 'http://187.217.116.231/testing/';
          break;
      case absUrl.startsWith('http://187.217.116.231/sistemas/'):
          $rootScope.ENVIRONMENT = 'production';
          $rootScope.baseUrl = 'http://187.217.116.231/sistemas/';
          break;
      default:
          $rootScope.ENVIRONMENT = 'development';
          $rootScope.baseUrl = 'http://localhost/';
          break;
  }

  let validar = (token) => {
    let d = $q.defer();

    LoginService.validateToken(token, response => {
      if (response.data.valido) {
        $rootScope.loggedIn = true;
        $rootScope.usuario = response.data.data;
        d.resolve(true);
      } else {
        localStorageService.remove('token');
        d.resolve(trans.router.stateService.target('login'));
      }
    });

    return d.promise;
  }

  $transitions.onBefore({to: 'inicio'}, trans => {

    let token = localStorageService.get('token');
    //Si existe el token entonces valida
    if (token) {
      $http.defaults.headers.common.Authorization = token;

      validar(token).then(valido => {
        if (!valido) {
          $state.transitionTo('login');
          return false;
        }
      })

    } else {
        $state.transitionTo('login');
        return false;
    }
  });

  $transitions.onBefore({to: 'login'}, trans => {
    let token = localStorageService.get('token');
    //Si existe el token entonces valida
    if (token) {
      $http.defaults.headers.common.Authorization = token;

      validar(token).then(valido => {
        if (valido) {
          $state.transitionTo('inicio');
          return false;
        }
      });
    }
  })
}

Run.$inject = ['$rootScope', 'localStorageService', '$state', 'LoginService', '$timeout', '$transitions', '$http', '$q', '$location'];

export default Run;
