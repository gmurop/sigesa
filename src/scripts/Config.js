'use strict';

import moment from 'moment';
import LoginCtrl from './controllers/LoginCtrl';
import MainCtrl from './controllers/MainCtrl';

export default function($mdThemingProvider, $mdDateLocaleProvider, $stateProvider, $urlRouterProvider, localStorageServiceProvider) {

  /*
    Routes
  */

  $urlRouterProvider.when('', '/');

  $stateProvider

    .state('login', {
      url: '/login',
      templateUrl: 'src/partials/login.html',
      controller: LoginCtrl,
    })

    .state('inicio', {
      url: '/',
      templateUrl: 'src/partials/inicio.html',
      controller: MainCtrl
    });

  /*
    Angular Material
  */

  let blueRevepss = $mdThemingProvider.extendPalette('blue', {
    '900': '294986'
  });

  $mdThemingProvider.definePalette('blueRevepss', blueRevepss);

  $mdThemingProvider.theme('default')
    .primaryPalette('blueRevepss', {
      'hue-1': '100',
      'hue-2': '300',
      'hue-3': '900'
    })
    .accentPalette('grey', {
      'default': '500',
      'hue-1': '200',
      'hue-2': '400',
      'hue-3': '800'
    });

    //Configuración del formato de la fecha

    $mdDateLocaleProvider.parseDate = dateString => {
      let m = moment(dateString, 'DD/MM/YYYY', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
    }

    $mdDateLocaleProvider.formatDate = date => {
      let m = moment(date);
      return m.isValid() ? m.format('DD/MM/YYYY') : '';
    }

  /*
    Local Storage
  */

  localStorageServiceProvider

    .setPrefix('sigesa')
    .setStorageType('sessionStorage');

}
