let fileModel = () => (
  {
      restrict: 'A',
      scope: {
          setFileData: '&'
      },
      link: function (scope, element, attrs) {
          //var model = $parse(attrs.fileModel);
          //var modelSetter = model.assign;
          element.bind('change', function () {
              scope.$apply(function () {
                  //modelSetter(scope, element[0].files[0]);
                  var val = element[0].files;
                  scope.setFileData({value: val});
              });
          });
      }
  }
);

export default fileModel;
