class CatalogosService {

  constructor($rootScope, $http) {
    this.$rootScope = $rootScope;
    this.$http = $http;
  }

  catEntidades(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/revepss/catalogos/entidades_federativas')
      .then(callback);
  }

  catMunicipios(claveEntidad, callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/revepss/catalogos/entidades_federativas/' + claveEntidad + '/municipios')
      .then(callback);
  }

  catLocalidades(claveEntidad, claveMunicipio, callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/revepss/catalogos/entidades_federativas/' + claveEntidad + '/municipios/' + claveMunicipio + '/localidades')
      .then(callback);
  }

  catUnidadesMedicas(claveEntidad, claveMunicipio, callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/revepss/catalogos/entidades_federativas/' + claveEntidad + '/municipios/' + claveMunicipio + '/unidades_salud')
      .then(callback);
  }

  catMediosContacto(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/solicitudes/medios_contacto')
      .then(callback);
  }

  catPrioridades(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/solicitudes/prioridades')
      .then(callback);
  }

  catCategorias(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/solicitudes/categorias')
      .then(callback);
  }

  catClasificaciones(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/solicitudes/clasificaciones')
      .then(callback);
  }

  catSubclasificaciones(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/solicitudes/subclasificaciones')
      .then(callback);
  }

  catCarteraServicios(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/revepss/catalogos/carteras_servicios_medicos')
      .then(callback);
  }

  catIntervenciones(idCartera, callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/revepss/catalogos/cartera/' + idCartera + '/intervenciones')
      .then(callback);
  }

  catActividadesPrincipales(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/solicitudes/actividades/principales')
      .then(callback);
  }

  catActividadesSecundarias(idActividadPrincipal, callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/solicitudes/actividades/principales/' + idActividadPrincipal + '/secundarias')
      .then(callback);
  }

  catParentescos(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/revepss/catalogos/parentescos')
      .then(callback);
  }

  catUnidadesAdministrativas(callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/catalogos/unidades_administrativas_derivar')
      .then(callback);
  }

}

CatalogosService.$inject = ['$rootScope', '$http'];

export default CatalogosService;
