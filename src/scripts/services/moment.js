import moment from '../../../node_modules/moment/moment';
import es from '../../../node_modules/moment/locale/es';

export default function momentJs() {
  moment.updateLocale('es', es);

  return moment;
}
