import {
  ENTIDAD_DEFAULT,
  MEDIO_CONTACTO_DEFAULT,
  PRIORIDAD_DEFAULT,
  CATEGORIA_DEFAULT,
  CLASIFICACION_DEFAULT,
  CARTERA_DEFAULT
 } from '../../constants';

export default function solicitud () {

  let listaSolicitudes = [];

  let cargarSolicitud = data => {
    solicitud = {
      ...solicitud,
      fechaEvento: data.fecha_evento,
    }
  }

  let agregarSolicitud = (solicitud = {
    idSolicitud: null,
    ticket: null,
    fechaEvento: new Date(),
    unidadMedica: '',
    entidad: ENTIDAD_DEFAULT,
    municipio: '',
    localidad: '',
    medioContacto: MEDIO_CONTACTO_DEFAULT,
    prioridad: PRIORIDAD_DEFAULT,
    descripcion: '',
    clasificacion: CLASIFICACION_DEFAULT,
    subclasificacion: '',
    categoria: CATEGORIA_DEFAULT,
    carteraServicios: CARTERA_DEFAULT,
    intervencion: '',
    actividadPrincipal: '',
    actividadesSecundarias: [],
    personas: [{
      idPersona: null,
      solicitante: true,
      contactoPrincipal: false,
      primerApellido: '',
      segundoApellido: '',
      nombre: '',
      sexo: 'H',
      fechaNacimiento: '',
      claveEntidadNacimiento: ENTIDAD_DEFAULT,
      curp: '',
      domicilio: {
        claveEntidad: ENTIDAD_DEFAULT,
        claveMunicipio: '',
        claveLocalidad: '',
        domicilio: ''
      },
      telefonoFijo: '',
      telefonoMovil: '',
      correo1: '',
      correo2: '',
      folioSP: '',
      folioSPIntegrante: '',
      parentesco: '',
    }]
  }) => {
    //console.log(solicitud);
    listaSolicitudes.push(solicitud);
  }

  let eliminarSolicitud = index => {
    listaSolicitudes.splice(index, 1);
  }

  let agregarPersona = (indexSolicitud, solicitante = true, persona) => {
    let p = (persona === undefined) ? {
      idPersona: null,
      solicitante: solicitante,
      contactoPrincipal: false,
      primerApellido: '',
      segundoApellido: '',
      nombre: '',
      sexo: 'H',
      fechaNacimiento: '',
      claveEntidadNacimiento: ENTIDAD_DEFAULT,
      curp: '',
      domicilio: {
        claveEntidad: ENTIDAD_DEFAULT,
        claveMunicipio: '',
        claveLocalidad: '',
        domicilio: ''
      },
      telefonoFijo: '',
      telefonoMovil: '',
      correo1: '',
      correo2: '',
      folioSP: '',
      folioSPIntegrante: '',
      parentesco: '',
    } : persona;

    listaSolicitudes[indexSolicitud].personas.push(p);
  }

  let eliminarPersona = (indexSolicitud, indexPersona) => {
    listaSolicitudes[indexSolicitud].personas.splice(indexPersona, 1);
  }

  var coincidenciasPersonas = [];

  let agregarCoincidenciasPersonas = personas => {
    coincidenciasPersonas.splice(0, coincidenciasPersonas.length);
    coincidenciasPersonas.splice(0, 0, ...personas);
  }

  let seleccionarPersona = (solicitudIndex, personaIndex, persona) => {
    listaSolicitudes[solicitudIndex].personas[personaIndex] = persona;
  }

  let generarObjetoPersona = (persona, i) => ({
      idPersona: persona.id_persona,
      solicitante: i === 0,
      contactoPrincipal: i > 0 && persona.contacto_principal === '1',
      primerApellido: persona.primer_apellido,
      segundoApellido: persona.segundo_apellido,
      nombre: persona.nombre,
      sexo: persona.sexo,
      fechaNacimiento: persona.fecha_nacimiento,
      claveEntidadNacimiento: persona.clave_entidad_federativa_nacimiento,
      entidadNacimiento: persona.entidad_federativa_nacimiento,
      curp: persona.curp,
      domicilio: {
        claveEntidad: persona.clave_entidad_federativa,
        entidad: persona.entidad_federativa_domicilio,
        claveMunicipio: persona.clave_municipio,
        municipio: persona.municipio_domicilio,
        claveLocalidad: persona.clave_localidad,
        localidad: persona.localidad_domicilio,
        domicilio: persona.domicilio
      },
      telefonoFijo: persona.telefono_fijo,
      telefonoMovil: persona.telefono_movil,
      correo1: persona.correo_electronico_01,
      correo2: persona.correo_electronico_02,
      folioSP: persona.folio_seguro_popular,
      folioSPIntegrante: Number(persona.folio_seguro_popular_id_integrante),
      parentesco: persona.id_parentesco,
      parentescoDescripcion: angular.lowercase(persona.parentesco)
    });

  let actualizarSolicitud = (index, solicitud) => {

    let solicitudActualizada = {
      ...listaSolicitudes[index],
      ...solicitud
    }

    listaSolicitudes[index] = solicitudActualizada;

    return listaSolicitudes[index];
  }

  return {
    agregarPersona,
    eliminarPersona,
    agregarSolicitud,
    listaSolicitudes,
    agregarCoincidenciasPersonas,
    coincidenciasPersonas,
    seleccionarPersona,
    generarObjetoPersona,
    actualizarSolicitud,
    eliminarSolicitud
  }

}
