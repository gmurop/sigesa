class LoginService {

  constructor($rootScope, $http) {
    this.$rootScope = $rootScope;
    this.$http = $http;
  }

  login(username, password, callback) {
    this.$http.post(this.$rootScope.baseUrl + 'api/login', {
      username,
      password
    })
    .then(callback);
  }

  validateToken(token, callback) {
    this.$http.get(this.$rootScope.baseUrl + 'api/token')
      .then(callback);
  }

}

LoginService.$inject = ['$rootScope', '$http'];

export default LoginService;
