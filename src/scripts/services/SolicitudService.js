class SolicitudService {

    constructor($rootScope, $http) {
        this.$rootScope = $rootScope;
        this.$http = $http;

    }

    nuevaSolicitud(solicitud, callback) {
        this.$http.post(this.$rootScope.baseUrl + 'api/sigesa/solicitudes/', solicitud)
            .then(callback);
    }

    obtenerSolicitudes(callback, params = {}, idSolicitud = '') {
        this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/solicitudes/' + idSolicitud, { params })
            .then(callback);
    }

    guardarNuevaPersona(idSolicitud, persona, callback) {
        this.$http.post(this.$rootScope.baseUrl + 'api/sigesa/personas', { ...persona, idSolicitud })
            .then(callback);
    }

    guardarPersonaExistente(idSolicitud, persona, callback) {
        this.$http.put(this.$rootScope.baseUrl + 'api/sigesa/personas', { ...persona, idSolicitud })
            .then(callback);
    }

    actualizarSolicitud(solicitud, callback) {
        this.$http.put(this.$rootScope.baseUrl + 'api/sigesa/solicitudes', { ...solicitud, carteraServicios: solicitud.cartera, personas: [] })
            .then(callback);
    }

    eliminarContacto(idSolicitud, idPersona, callback) {
        this.$http.delete(this.$rootScope.baseUrl + 'api/sigesa/solicitudes/' + idSolicitud + '/personas/' + idPersona)
            .then(callback);
    }

    buscarSolicitudes(search, callback) {
        this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/solicitudes/?search=' + search)
            .then(callback);
    }

    buscarPersona(curp, primerApellido, segundoApellido, nombre, callback) {
        this.$http.get(this.$rootScope.baseUrl + `api/sigesa/personas/?curp=${curp}&primerApellido=${primerApellido}&segundoApellido=${segundoApellido}&nombre=${nombre}`)
            .then(callback);
    }

    escalarSolicitud(idSolicitud, callback) {
        this.$http.post(this.$rootScope.baseUrl + `api/sigesa/solicitudes/${idSolicitud}/escaladas`)
            .then(callback);
    }

    finalizarSolicitud(idSolicitud, callback) {
        this.$http.put(this.$rootScope.baseUrl + `api/sigesa/solicitudes/${idSolicitud}/finalizar`)
            .then(callback);
    }

    cancelarSolicitudEscalada(idSolicitud, callback) {
        this.$http.delete(this.$rootScope.baseUrl + `api/sigesa/solicitudes/${idSolicitud}/escaladas`)
            .then(callback);
    }

    derivarSolicitud(idSolicitud, unidades, observaciones, callback) {
        this.$http.post(this.$rootScope.baseUrl + `api/sigesa/solicitudes/${idSolicitud}/derivadas`, { unidades, observaciones })
            .then(callback);
    }

    eliminarDerivacionSolicitud(idSolicitud, idUnidad, callback) {
        this.$http.delete(this.$rootScope.baseUrl + `api/sigesa/solicitudes/${idSolicitud}/derivadas/unidad/${idUnidad}`)
            .then(callback);
    }

    obtenerDerivaciones(idSolicitud, callback) {
        this.$http.get(this.$rootScope.baseUrl + `api/sigesa/solicitudes/${idSolicitud}/derivadas`)
            .then(callback);
    }

    agregarDocumentacion(idSolicitud, observacion, archivos = [], callback) {
        var fd = new FormData();
        fd.append('observacion', observacion);

        for (let i = 0; i <= archivos.length - 1; i++) {
            fd.append('archivo' + (i + 1), archivos[i]);
        }
        this.$http.post(this.$rootScope.baseUrl + `api/sigesa/seguimiento/solicitud/${idSolicitud}`, fd, {
                withCredencials: false,
                transformRequest: angular.identity,
                headers: {
                    'Content-type': undefined
                }
            })
            .then(callback);
    }

    editarDocumentacion(idSolicitud, idObservacion, observacion, archivos = [], callback) {
        var fd = new FormData();
        fd.append('observacion', observacion);

        for (let i = 0; i <= archivos.length - 1; i++) {
            fd.append('archivo' + (i + 1), archivos[i]);
        }
        this.$http.post(this.$rootScope.baseUrl + `api/sigesa/seguimiento/solicitud/${idSolicitud}/observaciones/${idObservacion}`, fd, {
                withCredencials: false,
                transformRequest: angular.identity,
                headers: {
                    'Content-type': undefined
                }
            })
            .then(callback);
    }

    obtenerDocumentacionSoporte(idSolicitud, callback) {
        this.$http.get(this.$rootScope.baseUrl + `api/sigesa/seguimiento/solicitud/${idSolicitud}/observaciones`)
            .then(callback);
    }

    obtenerBitacora(idSolicitud, callback) {
        this.$http.get(this.$rootScope.baseUrl + `api/sigesa/seguimiento/solicitud/${idSolicitud}`)
            .then(callback);
    }

    obtenerContadorSolicitudes(callback) {
        this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/contador')
            .then(callback);
    }

    reabrirSolicitud(idSolicitud, callback) {
        this.$http.put(this.$rootScope.baseUrl + `api/sigesa/solicitudes/${idSolicitud}/reabrir`)
            .then(callback);
    }

    validarSeguimiento(accion, idSolicitud, idObservacion, callback) {
        this.$http.put(this.$rootScope.baseUrl + `api/sigesa/seguimiento/solicitud/${idSolicitud}/observaciones/${idObservacion}/validar/${accion}`)
            .then(callback);
    }

    obtenerObservacionSeguimiento(idSolicitud, idObservacion, callback) {
        this.$http.get(this.$rootScope.baseUrl + `api/sigesa/seguimiento/solicitud/${idSolicitud}/observaciones/${idObservacion}`)
            .then(callback);
    }

    obtenerSeguimientosNoValidadosSolicitudes(callback) {
        this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/seguimiento/no_validado')
            .then(callback);
    }

    obtenerDesgloceIncidencias(callback, params = {}) {
        this.$http.get(this.$rootScope.baseUrl + 'api/sigesa/reportes/desgloce', { params })
            .then(callback);
    }

    obtenerTablaGrafica(mes, parametro, callback) {
        this.$http.get(this.$rootScope.baseUrl + `api/sigesa/graficas/tabla/mes/${mes}`)
            .then(callback);
    }

    obtenerReporteSeguimientoExcel(parametros = {}, callback) {

        this.$http.post(this.$rootScope.baseUrl + 'api/sigesa/reportes/seguimientos/excel', parametros)
            .then(callback);
    }

    agregarFusa(idSolicitud, fusa, callback) {
        this.$http.post(this.$rootScope.baseUrl + 'api/sigesa/solicitudes/fusa', { ...fusa, idSolicitud: idSolicitud})
            .then(callback);
    }
    cabiarContrasenia(password, newPassword, callback){
        this.$http.post(this.$rootScope.baseUrl + 'api/sigesa/settings', {password: password, newPassword : newPassword})
        .then(callback);

    }

}

SolicitudService.$inject = ['$rootScope', '$http'];

export default SolicitudService;