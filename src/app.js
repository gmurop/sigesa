'use strict';

import * as ng from 'angular';
import uiRouter from 'angular-ui-router';
import ngMaterial from 'angular-material';
import LocalStorageModule from 'angular-local-storage';
import { ngTableModule } from 'ng-table';
import cgNotify from 'angular-notify';

import Config from './scripts/Config';
import Run from './scripts/Run';

/* Controllers */
import LoginCtrl from './scripts/controllers/LoginCtrl';
import MainCtrl from './scripts/controllers/MainCtrl';
import SolicitudCtrl from './scripts/controllers/SolicitudCtrl';
import SolicitanteFormCtrl from './scripts/controllers/SolicitanteFormCtrl';
import SolicitudFormCtrl from './scripts/controllers/SolicitudFormCtrl';
import SolicitudesCtrl from './scripts/controllers/SolicitudesCtrl';
import FiltrarSolicitudesCtrl from './scripts/controllers/FiltrarSolicitudesCtrl';
import SeguimientoCtrl from './scripts/controllers/SeguimientoCtrl';
import DialogPersonasCtrl from './scripts/controllers/DialogPersonasCtrl';
import DialogDerivarSolicitud from './scripts/controllers/DialogDerivarSolicitud';
import ResumenCtrl from './scripts/controllers/ResumenCtrl';
import PrincipalJefeCtrl from './scripts/controllers/PrincipalJefeCtrl';
import OpcionesReportesCtrl from './scripts/controllers/OpcionesReportesCtrl';
import Reporte1Ctrl from './scripts/controllers/Reporte1Ctrl';
import FusaCtrl from './scripts/controllers/FusaCtrl';
import ContraseniaCtrl from './scripts/controllers/ContraseniaCtrl';

/* Factories */
import CatalogosService from './scripts/services/CatalogosService';
import SolicitudService from './scripts/services/SolicitudService';
import LoginService from './scripts/services/LoginService';
import solicitud from './scripts/services/solicitud';
import momentJs from './scripts/services/moment';

/* Filters */
import { excerpt } from './filters';

/* Directives */
import fileModel from './scripts/directives/fileModel';

ng.module('sigesaApp', [uiRouter, ngMaterial, LocalStorageModule, ngTableModule.name, cgNotify])
    .config(['$mdThemingProvider', '$mdDateLocaleProvider', '$stateProvider', '$urlRouterProvider', 'localStorageServiceProvider', Config])
    .run(Run)
    .controller('LoginCtrl', LoginCtrl)
    .controller('MainCtrl', MainCtrl)
    .controller('SolicitudCtrl', SolicitudCtrl)
    .controller('SolicitanteFormCtrl', SolicitanteFormCtrl)
    .controller('SolicitudFormCtrl', SolicitudFormCtrl)
    .controller('SolicitudesCtrl', SolicitudesCtrl)
    .controller('FiltrarSolicitudesCtrl', FiltrarSolicitudesCtrl)
    .controller('SeguimientoCtrl', SeguimientoCtrl)
    .controller('DialogPersonasCtrl', DialogPersonasCtrl)
    .controller('DialogDerivarSolicitud', DialogDerivarSolicitud)
    .controller('ResumenCtrl', ResumenCtrl)
    .controller('PrincipalJefeCtrl', PrincipalJefeCtrl)
    .controller('OpcionesReportesCtrl', OpcionesReportesCtrl)
    .controller('Reporte1Ctrl', Reporte1Ctrl)
    .controller('FusaCtrl', FusaCtrl)
    .controller('ContraseniaCtrl', ContraseniaCtrl)
    .service('CatalogosService', CatalogosService)
    .service('SolicitudService', SolicitudService)
    .service('LoginService', LoginService)
    .factory('solicitud', solicitud)
    .factory('moment', momentJs)
    .filter('excerpt', excerpt)
    .directive('fileModel', fileModel)
    .directive('uppercase', () => {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: (scope, element, attrs, ctrl) => {

                let capitalize = (input = '') => {
                    let capitalized = angular.uppercase(input);

                    if (input !== capitalized) {
                        ctrl.$setViewValue(capitalized);
                        ctrl.$render();
                    }

                    return capitalized;
                }

                ctrl.$parsers.unshift(capitalize);
                capitalize(scope[attrs.ngModel]);
            }
        }
    })
    .directive('autofocus', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                $timeout(function() {
                    $element[0].focus();
                });
            }
        }
    }]);