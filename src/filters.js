export function excerpt()  {
  return (input, size) => {
    input = input || "";
    size = size || 30;
    return (input.length >= size) ? input.substring(0, size) + '...' : input;
  };
}
