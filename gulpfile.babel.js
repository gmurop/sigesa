'use strict';

import browserify from 'browserify';
import watchify from 'watchify';
import gulp from 'gulp';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import uglify from 'gulp-uglify';
import gutil from 'gulp-util';
import babelify from 'babelify';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import watch from 'gulp-watch';
import browserSync, {reload} from 'browser-sync';
import versionAppend from 'gulp-version-append';
import path from 'path';
import minifyHtml from 'gulp-minify-html';
import rev from 'gulp-rev-append';
//import ngAnnotate from 'gulp-ng-annotate';

gulp.task('styles', () => {
  return gulp.src('./src/styles/main.scss')
    .pipe(watch('./src/styles/main.scss'))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'))
    .pipe(reload({stream: true}));
});

let b = browserify({
    entries: './src/app.js',
    debug: true,
    cache: {},
    packageCache: {},
    plugin: [watchify]
  }).transform(babelify);

  b.on("update", () => {
    return bundle(b);
  });
  b.on("log", msg => {
      console.log(msg);
    });

gulp.task('javascript', () => {
  return bundle(b);
});


let bundle = (b) => {

  return b.bundle()
    .on('error', err => {gutil.log('Browserify error', gutil.colors.red(err.message))})
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())//Esta descoméntala cuando vayas a subir a producción, o déjala descomentada, como tu veas
    .pipe(sourcemaps.init({loadMaps: true}))
    //.pipe(ngAnnotate())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/js'))
    .pipe(reload({stream: true}));
};

gulp.task('serve', () => {
  browserSync.init({
    server: {
      baseDir: './'
    }
  });

  gulp.watch(['../**/*.html', './**/*.scss', './**/*.js'], {cwd: './src'}, ['styles', 'javascript']);
  //gulp.watch(['../**/*.html', './**/*.js'], {cwd: './src'}, ['javascript']);
});

gulp.task('html', function(){
    return gulp.src('./index.html')
          .pipe(versionAppend(['html', 'js', 'css'], {appendType: 'guid', versionFile: 'version.json'}))
          .pipe(minifyHtml());
});

gulp.task('rev', function() {
  gulp.src('./index.html')
    .pipe(rev())
    .pipe(gulp.dest('.'));
});
